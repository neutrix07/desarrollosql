/*Esta es una forma de crear tablas y setear los atributos por campo*/
CREATE TABLE estudiantes_beca(
    id_estudiante NUMBER(11) PRIMARY KEY,/*llave primaria*/
    nombre VARCHAR2(30) NOT NULL,/*campo no nulo*/ 
    a_paterno VARCHAR2(30) NOT NULL,
    a_materno VARCHAR2(30),
    fecha_nacimiento DATE
)

/*esta es la segunda forma de hacer los constraint*/
CREATE TABLE cursos_beca(
    id_curso NUMBER(11),
    nombre_beca VARCHAR2(50) NOT NULL,
    fecha_alta TIMESTAMP DEFAULT SYSDATE,/*automatico fecha de alta*/
    CONSTRAINT becas_pk PRIMARY KEY(id_curso),/*llave primaria*/
    CONSTRAINT valor_unico_fecha_beca UNIQUE(fecha_alta)
)


CREATE TABLE profesores_beca(
    id_profesor NUMBER(4),
    a_paterno_profesor VARCHAR2(30),
    a_materno_profesor VARCHAR2(30),
    fecha_alta_profesro TIMESTAMP DEFAULT SYSDATE NOT NULL,
    
)